/*
  Warnings:

  - Added the required column `birthdate` to the `Reservee` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Reservee" ADD COLUMN     "birthdate" TIMESTAMP(3) NOT NULL;
