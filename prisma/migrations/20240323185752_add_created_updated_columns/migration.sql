-- CreateTable
CREATE TABLE "Reservee" (
    "id" TEXT NOT NULL,
    "title" TEXT NOT NULL,
    "gender" TEXT,
    "firstName" TEXT,
    "lastName" TEXT,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,
    "reserverId" TEXT,

    CONSTRAINT "Reservee_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "users" (
    "id" TEXT NOT NULL,
    "name" TEXT,
    "email" TEXT,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,

    CONSTRAINT "users_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "users_email_key" ON "users"("email");

-- AddForeignKey
ALTER TABLE "Reservee" ADD CONSTRAINT "Reservee_reserverId_fkey" FOREIGN KEY ("reserverId") REFERENCES "users"("id") ON DELETE SET NULL ON UPDATE CASCADE;
