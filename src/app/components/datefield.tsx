import { DateValidationError, PickerChangeHandlerContext } from '@mui/x-date-pickers';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import { Dayjs } from 'dayjs';

interface DatefieldProps {
  value: Dayjs;
  onChange: (value: Dayjs | null, context: PickerChangeHandlerContext<DateValidationError>) => void;
}

const Datefield = ({ value, onChange }: DatefieldProps) => {
  return <DatePicker value={value} onChange={onChange} />
}

export default Datefield;
