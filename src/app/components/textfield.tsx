import React from 'react';
import TextField from '@mui/material/TextField';

interface TextfieldProps {
  label?: string;
  placeholder?: string;
  name: string;
  type: "text" | "birthdate" | "number" | "email" | "password";
  value: any;
  onChange?: (e: React.ChangeEvent<HTMLInputElement | HTMLSelectElement>) => void;
}

const Textfield = ({ label, placeholder, name, type, value, onChange }: TextfieldProps) => {
  return (
    <TextField
      label={label}
      name={name}
      variant="standard"
      value={value}
      onChange={onChange}
      type={type}
      placeholder={placeholder}
    />
  )
}

export default Textfield
