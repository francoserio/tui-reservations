import React from 'react';
import Button from '@mui/material/Button';

interface ButtonfieldProps {
  type: "submit" | "button"
  disabled?: boolean;
  label: string;
  onClick?: Function
}

const Buttonfield = ({ type, disabled, label, onClick }: ButtonfieldProps) => {
  return (
    <Button variant="outlined" type={type} disabled={disabled} className="m-4" onClick={onClick}>{label}</Button>
  )

}

export default Buttonfield;

