import Select from '@mui/material/Select';
import MenuItem from '@mui/material/MenuItem';
import { SelectChangeEvent } from "@mui/material";

interface Option {
  value: string;
  label: string;
}

interface SelectProps {
  options: Array<Option>
  value: any;
  onChange?: (e: SelectChangeEvent<any>) => void;
  label: string;
  name: string;
}

const Selectfield = ({ name, label, options, value, onChange }: SelectProps) => {
  const listOptions = options.map((option, index) => <MenuItem key={index} value={option.value}>{option.label}</MenuItem>)
  return (
    <Select
      name={name}
      value={value}
      label={label}
      onChange={onChange}
    >
      {listOptions}
    </Select>
  )
}

export default Selectfield;
