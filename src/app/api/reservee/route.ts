import { NextResponse, NextRequest } from "next/server";
import prisma from '@/lib/prisma'
import { cookies } from "next/headers";
import { createRouteHandlerClient } from "@supabase/auth-helpers-nextjs";
import { Database } from "@/lib/database.types";
import dayjs from "dayjs";

interface Passenger {
  title: string;
  gender: string;
  firstName: string;
  lastName: string;
  birthdate: string;
}

export async function GET() {
  const cookieStore = cookies();
  const supabase = createRouteHandlerClient<Database>({
    cookies: () => cookieStore,
  });

  const { data: { user } } = await supabase.auth.getUser();
  const userId = user?.id || ''

  const reservees = await prisma.reservee.findMany({
    where: {
      reserverId: userId,
    },
  })
  const latestUpdated = await prisma.reservee.findFirst({
    where: {
      reserverId: userId,
    },
    orderBy: {
      updatedAt: 'desc',
    },
  })

  const notAlreadyModified = dayjs(latestUpdated?.createdAt).isSame(dayjs(latestUpdated?.updatedAt))
  const passengers = reservees.reduce((result, value) => ({
    ...result,
    [value.id]: {
      title: value.title,
      gender: value.gender,
      firstName: value.firstName,
      lastName: value.lastName,
      birthdate: value.birthdate
    }
  }), {})

  return NextResponse.json({ message: 'Fetched successfully!', passengers, notAlreadyModified }, { status: 200 })
}

export async function POST(request: NextRequest) {
  const cookieStore = cookies();
  const supabase = createRouteHandlerClient<Database>({
    cookies: () => cookieStore,
  });

  const { data: { user } } = await supabase.auth.getUser();
  const userId = user?.id || ''

  const { passengers }: { passengers: Array<Passenger> } = await request.json();
  const promises = Object.entries(passengers).map(([key, value]) => prisma.reservee.upsert({
    where: {
      id: key,
      reserverId: userId
    },
    create: {
      title: value.title,
      gender: value.gender,
      birthdate: value.birthdate,
      firstName: value.firstName,
      lastName: value.lastName,
      reserverId: userId
    },
    update: {
      title: value.title,
      gender: value.gender,
      birthdate: value.birthdate,
      firstName: value.firstName,
      lastName: value.lastName
    }
  }))
  await Promise.all(promises)
  return NextResponse.json({ message: "Saved successfully!", passengers }, { status: 200 });
}
