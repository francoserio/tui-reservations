'use client'

import { v4 } from 'uuid';
import dayjs, { Dayjs } from 'dayjs';
import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/navigation';

import { createClientComponentClient } from '@supabase/auth-helpers-nextjs';
import { LocalizationProvider } from '@mui/x-date-pickers';
import { AdapterDayjs } from '@mui/x-date-pickers/AdapterDayjs'
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import CircularProgress from '@mui/material/CircularProgress';

import { isFormCompleted } from '@/app/helpers/isFormCompleted'
import Buttonfield from '@/app/components/buttonfield';
import PassengerForm from '@/app/forms/passengerForm';

interface Passenger {
  title: string;
  gender: string;
  firstName: string;
  lastName: string;
  birthdate: Dayjs;
}

interface IPassenger {
  [key: string]: Passenger
}

export default function Home() {
  const router = useRouter();
  const supabase = createClientComponentClient();
  const [loading, setLoading] = useState<boolean>(false)
  const [passengers, setPassengers] = useState<IPassenger>({})
  const [notAlreadyModified, setNotAlreadyModified] = useState<boolean>(true)
  const [disableSave, setDisableSave] = useState<boolean>(true)
  const [confirmationOpened, setConfirmationOpen] = useState<boolean>(false)

  useEffect(() => {
    (async () => {
      setLoading(true)
      const res = await fetch('/api/reservee', {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        },
      })
      const { passengers: serverPassengers, notAlreadyModified: serverNotAlreadyModified } = await res.json()
      setPassengers(serverPassengers)
      setNotAlreadyModified(serverNotAlreadyModified)
      setLoading(false)
    })();
  }, [])

  const deletePassenger = (key: string) => {
    const { [key]: value, ...rest } = passengers;
    setPassengers(rest);
  }

  const handleClickOpen = () => {
    setConfirmationOpen(true);
  };

  const handleClose = () => {
    setConfirmationOpen(false);
  };

  const handleSignOut = async () => {
    await supabase.auth.signOut();
    router.push('/login');
  };

  const addPassenger = () => {
    const id = v4()
    setPassengers({
      ...passengers,
      [id]: {
        title: '',
        gender: '',
        firstName: '',
        lastName: '',
        birthdate: dayjs().subtract(18, 'year')
      }
    })
  }

  const onChange = (newForm: IPassenger) => {
    setPassengers({
      ...passengers,
      ...newForm
    })
  }

  const savePassengers = async () => {
    setLoading(true)
    const res = await fetch('/api/reservee', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ passengers: Object.values(passengers) })
    })
    const result = await res.json()
    setPassengers(result.passengers)
    handleClose()
    setLoading(false)
  }

  const atLeastOneComplete = () => {
    return Object.entries(passengers).length > 0 && Object.values(passengers).map(passenger => isFormCompleted(passenger)).every(Boolean)
  }

  const canSave = () => {
    console.log(atLeastOneComplete(), notAlreadyModified)
    return atLeastOneComplete() && notAlreadyModified
  }

  useEffect(() => {
    setDisableSave(!canSave())
  }, [passengers])

  return (
    <LocalizationProvider dateAdapter={AdapterDayjs}>
      <main className="flex in-h-screen flex-col items-center p-24">
        <div className="space-y-4 m-4">
          {loading
            ?
            <CircularProgress />
            :
            Object.entries(passengers).map(([id, passenger]) => <PassengerForm
              id={id}
              key={id}
              title={passenger.title}
              gender={passenger.gender}
              firstName={passenger.firstName}
              lastName={passenger.lastName}
              birthdate={dayjs(passenger.birthdate)}
              onDelete={() => deletePassenger(id)}
              onChange={(newForm: IPassenger) => onChange(newForm)}
            />)}
        </div>
        <div className="space-y-4 flex flex-col">
          <Buttonfield label="Add Passenger" type="button" onClick={addPassenger} />
          <Buttonfield type="button" label="Save" onClick={handleClickOpen} disabled={disableSave} />
        </div>
        <Dialog
          open={confirmationOpened}
          onClose={handleClose}
        >
          <DialogTitle>Confirm?</DialogTitle>
          {Object.values(passengers).map((passenger, index) => <div key={index} className="p-4">
            <p>Passenger {index + 1}</p>
            <p>{passenger.title.toUpperCase()} {passenger.firstName} {passenger.lastName}</p>
            <p>and {passenger.gender === 'male' ? 'his' : 'her'} birthdate is on the {dayjs(passenger.birthdate).format('DD/MM/YYYY')}</p>
          </div>)}
          {loading
            ?
            <CircularProgress />
            :
            <div className="p-4 flex flex-row space-x-4">
              <Buttonfield label="Cancel" type="button" onClick={handleClose} />
              <Buttonfield label="Ok" type="button" onClick={savePassengers} />
            </div>
          }
        </Dialog>
        <button onClick={handleSignOut}>Sign out</button>
      </main>
    </LocalizationProvider>
  );
}
