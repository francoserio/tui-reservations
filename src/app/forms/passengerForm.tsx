import React, { useState } from 'react';
import dayjs, { Dayjs } from 'dayjs';
import { SelectChangeEvent } from "@mui/material";

import Textfield from '@/app/components/textfield';
import Selectfield from '@/app/components/selectfield';
import Datefield from '@/app/components/datefield';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';

interface PassengerForm {
  title: string;
  gender: string;
  firstName: string;
  lastName: string;
  birthdate: Dayjs;
}

interface IPassenger {
  [key: string]: PassengerForm
}

interface PassengerProps extends PassengerForm {
  onDelete?: React.MouseEventHandler<HTMLButtonElement>;
  onChange: (newForm: IPassenger) => void;
  id: string;
}

const PassengerForm = ({ id, title, gender, firstName, lastName, birthdate, onDelete, onChange }: PassengerProps) => {
  const [formData, setFormData] = useState<PassengerForm>({
    title: title || 'mr',
    gender: gender || 'male',
    firstName: firstName || '',
    lastName: lastName || '',
    birthdate: birthdate || dayjs().subtract(18, 'year'),
  });

  const handleChangeDate = (value: Dayjs | null) => {
    if (value) {
      setFormData({
        ...formData,
        birthdate: value
      })
      onChange({
        [id]: formData
      })
    }
  }

  const handleChange = (e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement> | SelectChangeEvent<any>) => {
    const { name, value } = e.target;
    const newForm = {
      ...formData,
      [name]: value,
    }
    setFormData(newForm);
    onChange({
      [id]: newForm
    })
  };

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    // Handle form submission here
    console.log(formData);
  };

  const titleOptions = [
    {
      value: 'mr',
      label: "Mr."
    },
    {
      value: "mrs",
      label: "Mrs."
    }
  ];

  const genderOptions = [
    {
      value: "male",
      label: "M"
    },
    {
      value: "female",
      label: "F"
    }
  ]

  return (
    <form onSubmit={handleSubmit} className="p-8 rounded-md border-black border-solid border-2 space-y-4">
      <div className="flex flex-row space-x-4">
        <Textfield type="text" label="First name" name="firstName" value={formData.firstName} onChange={handleChange} />
        <Textfield type="text" label="Last name" name="lastName" value={formData.lastName} onChange={handleChange} />
        <IconButton aria-label="delete" onClick={onDelete}>
          <DeleteIcon />
        </IconButton>
      </div>
      <div className="flex flex-row space-x-4">
        <Selectfield name="title" label="Title" value={formData.title} onChange={handleChange} options={titleOptions} />
        <Selectfield name="gender" label="Gender" value={formData.gender} onChange={handleChange} options={genderOptions} />
        <Datefield value={formData.birthdate} onChange={handleChangeDate} />
      </div>
    </form>
  );
};

export default PassengerForm;
