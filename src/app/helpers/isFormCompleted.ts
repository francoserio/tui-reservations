import dayjs, { Dayjs } from 'dayjs';

interface PassengerProps {
  title: string;
  gender: string;
  firstName: string;
  lastName: string;
  birthdate: Dayjs;
}

export const isFormCompleted = ({ title, gender, firstName, lastName, birthdate }: PassengerProps) => {
  return title && gender && firstName && lastName && dayjs(birthdate).isBefore(dayjs())
}
