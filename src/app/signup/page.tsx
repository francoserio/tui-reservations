"use client";

import { createClientComponentClient } from "@supabase/auth-helpers-nextjs";
import { useRouter } from "next/navigation";
import { useState } from "react";

import Textfield from '@/app/components/textfield';
import type { Database } from "@/lib/database.types";

export default function SignUp() {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const router = useRouter();
  const supabase = createClientComponentClient<Database>();

  const handleSignUp = async () => {
    await supabase.auth.signUp({
      email,
      password,
      options: {
        emailRedirectTo: `${location.origin}/auth/callback`,
      },
    });
    router.push('/dashboard');
  };

  return (
    <div className="flex flex-col m-4 w-1/2 p-4 mx-auto mb-0 mt-4 rounded-md border-black border-solid border-2 space-y-4">
      <Textfield
        type="email"
        name="email"
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => setEmail(e.target.value)}
        placeholder="Email"
        value={email}
      />
      <Textfield
        type="password"
        placeholder="Password"
        name="password"
        onChange={(e: React.ChangeEvent<HTMLInputElement>) => setPassword(e.target.value)}
        value={password}
      />
      <button onClick={handleSignUp}>Sign up</button>
    </div>
  );
}
